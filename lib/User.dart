class User {
  int id = 0;
  String name = '';
  String? avatarUrl;

  User({this.id = 0, this.name = 'User', this.avatarUrl});

  factory User.fromJson(Map<String, dynamic> json) {
    return User(
        id: json['id'] ?? 0,
        name: json['name'] ?? '',
        avatarUrl: json['avatar_urls']['24'] ?? '');
  }
}
