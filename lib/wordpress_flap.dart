library wordpress_flap;

import 'package:http/http.dart';
import 'dart:convert';

import 'Post.dart';
import 'Media.dart';
import 'User.dart';
import 'Category.dart';
import 'EventPost.dart';

export 'Post.dart';
export 'Media.dart';
export 'User.dart';
export 'Category.dart';
export 'EventPost.dart';

class Wordpress {
  String baseURL;
  String postfix = 'wp-json/wp/v2/';

  Wordpress(this.baseURL);

  Future<List<Post>> getPosts(PostFetchParams params) async {
    Map<String, dynamic> parameters = {
      'page': params.page.toString(),
      'per_page': params.perPage.toString(),
      'categories': params.categories!.join(',').toString(),
      'status': params.status,
      'order': params.order,
      'orderby': params.orderby
    };
    final response =
        await get(Uri.https(this.baseURL, this.postfix + 'posts', parameters));
    if (response.statusCode == 200) {
      List<Post> posts = (json.decode(response.body) as List)
          .map((i) => Post.fromJson(i))
          .toList();
      for (Post post in posts) {
        if (params.fetchUser) {
          post.author = await getUser(post.authorID);
        } else {
          post.author = User(name: "User");
        }
        if (params.fetchMedia) {
          if (post.mediaID != null && post.mediaID != 0) {
            post.featuredMedia = await getMedia(post.mediaID ?? 0) ?? Media();
          } else {
            post.featuredMedia = Media();
          }
        }
      }
      return posts;
    }
    print(response.statusCode.toString());
    throw StateError('No Data Returned.');
  }

  Future<Media?> getMedia(int id) async {
    final response = await get(
        Uri.https(this.baseURL, this.postfix + 'media/' + id.toString()));
    if (response.statusCode == 200) {
      Media media = Media.fromJson(json.decode(response.body));
      return media;
    } else {
      print('Could not fetch media');
    }
  }

  Future<User?> getUser(int id) async {
    final response = await get(
        Uri.https(this.baseURL, this.postfix + 'users/' + id.toString()));
    if (response.statusCode == 200) {
      User user = User.fromJson(json.decode(response.body));
      return user;
    } else {
      print('Could not fetch user');
    }
  }

  Future<List<Category>> getCategories(List<int> ids) async {
    final response = await get(Uri.https(
        this.baseURL,
        this.postfix + 'categories',
        {'include': ids.join(','), 'orderby': 'include', 'order': 'desc'}));
    if (response.statusCode == 200) {
      List<Category> categories = (jsonDecode(response.body) as List)
          .map((e) => Category.fromJson(e))
          .toList();
      return categories;
    }
    throw StateError('No Data Returned.');
  }

  Future<List<EventPost>> getEventPosts(EventPostFetchParams params) async {
    Map<String, dynamic> parameters = {
      'page': params.page.toString(),
      'per_page': params.perPage.toString(),
      'status': params.status,
      'order': params.order,
      'orderby': params.orderby
    };
    final response =
        await get(Uri.https(this.baseURL, this.postfix + 'events', parameters));
    if (response.statusCode == 200) {
      List<EventPost> posts = (json.decode(response.body) as List)
          .map((i) => EventPost.fromJson(i))
          .toList();
      for (EventPost post in posts) {
        if (params.fetchUser) {
          post.author = await getUser(post.authorID);
        } else {
          post.author = User(name: "User");
        }
        if (params.fetchMedia) {
          if (post.mediaID != null && post.mediaID != 0) {
            post.featuredMedia = await getMedia(post.mediaID ?? 0) ?? Media();
          } else {
            post.featuredMedia = Media();
          }
        }
      }
      return posts;
    }
    print(response.statusCode.toString());
    throw StateError('No Data Returned.');
  }
}
