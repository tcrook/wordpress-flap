class Category {
  int id = 0;
  String name = '';

  Category({this.id = 0, this.name = 'Category'});

  factory Category.fromJson(Map<String, dynamic> json) {
    return Category(
        id: json['id'] ?? 0,
        name: json['name'] ?? '');
  }
}