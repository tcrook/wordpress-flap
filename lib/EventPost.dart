import 'package:wordpress_flap/wordpress_flap.dart';

import 'Media.dart';
import 'User.dart';

class EventPost {
  int id;
  String? slug;
  String? link;
  String? title;
  String? excerpt;
  String? content;
  String? date;
  int? mediaID;
  Media? featuredMedia;
  int authorID;
  User? author;
  String? startDate;
  String? endDate;

  EventPost(
      {this.id = 0,
      this.slug,
      this.link,
      this.excerpt,
      this.title,
      this.content,
      this.date,
      this.mediaID,
      this.featuredMedia,
      this.authorID = 0,
      this.author,
      this.startDate,
      this.endDate});

  factory EventPost.fromJson(Map<String, dynamic> json) {
    return EventPost(
        id: json['id'],
        slug: json['slug'],
        link: json['link'],
        excerpt: json['excerpt']['rendered'],
        title: json['title']['rendered'],
        content: json['content']['rendered'],
        date: json['date'],
        mediaID: json['featured_media'],
        authorID: json['author'],
        startDate: json['meta']['ct_event_start'],
        endDate: json['meta']['ct_event_end']);
  }

  void fetchMedia(Wordpress wordpress) async {
    this.featuredMedia = await wordpress.getMedia(this.mediaID ?? 0) ?? Media();
  }

  void fetchAuthor(Wordpress wordpress) async {
    this.author = await wordpress.getUser(this.authorID) ?? User();
  }
}

class EventPostFetchParams {
  List<int>? categories;
  List<int>? categoriesExclude;
  List<int>? include;
  List<int>? exclude;
  int page = 1;
  int perPage = 10;
  String status = 'publish';
  String? slug;
  List<String>? tags;
  List<String>? tagsExclude;
  String order = 'desc'; // asc or desc
  String orderby = 'date';
  bool fetchMedia = true;
  bool fetchUser = true;

  EventPostFetchParams(
      {this.categories,
      this.categoriesExclude,
      this.include,
      this.exclude,
      this.page = 1,
      this.perPage = 10,
      this.status = 'publish',
      this.slug,
      this.tags,
      this.tagsExclude,
      this.order = 'desc',
      this.orderby = 'date',
      this.fetchMedia = true,
      this.fetchUser = true});
}
