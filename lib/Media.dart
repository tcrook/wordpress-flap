class Media {
  int id = 0;
  String? slug = '';
  String? url = '';

  Media({this.id = 0, this.slug, this.url});

  factory Media.fromJson(Map<String, dynamic> json) {
    return Media(
        id: json['id'] ?? 0,
        slug: json['slug'] ?? '',
        url: json['guid']['rendered'] ?? '');
  }
}
