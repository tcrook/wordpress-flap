import 'package:flutter_test/flutter_test.dart';
import 'package:wordpress_flap/Post.dart';

import 'package:wordpress_flap/wordpress_flap.dart';

void main() {
  test('Get posts', () async {
    final wordpress = Wordpress('svinews.com');
    PostFetchParams params = PostFetchParams(
        fetchMedia: false,
        categories: [1246, 1104, 8361],
        fetchUser: false,
        perPage: 20);
    List<Post> posts = await wordpress.getPosts(params);
    for (Post post in posts) {
      print(post.slug);
      print(post.featuredMedia?.url);
      print(post.author?.name);
    }
  });

  test('Get categories', () async {
    final wordpress = Wordpress('svinews.com');
    List<Category> categories = await wordpress.getCategories([1246, 1104]);
    for (Category category in categories) {
      print(category.name);
    }
  });

  test('Get Event Posts', () async {
    final wordpress = Wordpress('svinews.com');
    EventPostFetchParams params = EventPostFetchParams(
        fetchMedia: false,
        fetchUser: false,
        perPage: 20);
    List<EventPost> posts = await wordpress.getEventPosts(params);
    for (EventPost post in posts) {
      print(post.slug);
      print(post.featuredMedia?.url);
      print(post.author?.name);
      print(post.startDate);
      print(post.endDate);
    }
  });

}
